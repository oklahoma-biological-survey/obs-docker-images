import argparse
import jinja2
import json
import os, sys
import pathlib

JINJA_ENV      = jinja2.Environment(autoescape=True)
DIR            = os.path.dirname(os.path.abspath(__file__))
CONFIG_FILE    = os.path.join(DIR, "config.json")
GITLAB_CI_SRC  = os.path.join(DIR, "templates", ".gitlab-ci.yml.j2")
GITLAB_CI_DST  = os.path.join(DIR, ".gitlab-ci.yml")
DOCKERFILE_SRC = os.path.join(DIR, "templates", "Dockerfile.j2")
DOCKERFILE_DST = os.path.join(DIR, "name", "Dockerfile")

def write_template(src: str, dst: str, conf: dict) -> None:
  with open(src) as src_file:
    template = JINJA_ENV.from_string(src_file.read())
    with open(dst, "w") as dst_file:
      dst_file.write(template.render(conf))

def base_image_prompt(args: dict) -> None:
  if not args:
    print("Must provide args")
    sys.exit(1)

  base_image = ''

  input_str = input("Enter the base image for this image: ")

  # TODO: implement regex for base_image
  if len(input_str) > 0:
    base_image = input_str

  if not base_image:
    base_image = base_image_prompt(args)

  args["base_image"] = base_image

def name_prompt(args: dict) -> None:
  if not args:
    print("Must provide args")
    sys.exit(1)

  name = ''

  input_str = input("Enter the name of the maintainer of this image: ")

  if len(input_str) > 0:
    name = input_str

  if not name:
    name = name_prompt(args)

  args["name"] = name

def email_prompt(args: dict) -> None:
  if not args:
    print("Must provide args")
    sys.exit(1)

  email = ''

  input_str = input("Enter the email of the maintainer of this image: ")

  # TODO: implement regex for email
  if len(input_str) > 0:
    email = input_str

  if not email:
    email = email_prompt(args)

  args["email"] = email

def setup_parser() -> argparse.ArgumentParser:
  parser = argparse.ArgumentParser(description="Oklahoma Biological Survey Build Image Creator")

  parser.add_argument('-m', '--mode', help='Mode to operate in (create, delete, or update_all).', required=True)
  parser.add_argument('-i', '--image', help='Name of the image to create or delete.', required=False)
  parser.add_argument('-b', '--base-image', help='Base image to build from.', required=False)
  parser.add_argument('-n', '--name', help='Name of the maintainer of the image.', required=False)
  parser.add_argument('-e', '--email', help='Email of the maintainer of the image.', required=False)

  return parser

def process_cli_args(args: argparse.Namespace) -> dict:
  if not args:
    print("Must provide args")
    sys.exit(1)

  out = dict()

  if hasattr(args, "image") and args.image is not None:
    out["image"] = args.image
  else:
    if args.mode != "update_all":
      print("Must provide image name")
      sys.exit(1)

  if args.mode == "create" or args.mode == "delete" or args.mode == "update_all":
    out["mode"] = args.mode
  else:
    print("Mode is invalid (must be \"create\", \"delete\", or \"update_all\")")
    sys.exit(1)

  if out["mode"] == "create":
    if hasattr(args, "base_image"):
      out["base_image"] = args.base_image

    if hasattr(args, "name"):
      out["name"] = args.name

    if hasattr(args, "email"):
      out["email"] = args.email

  return out

def load_config() -> dict:
  global CONFIG_FILE

  if len(CONFIG_FILE) > 1:
    path = pathlib.Path(CONFIG_FILE)
    with open(path.expanduser()) as config_file:
      config = json.load(config_file)
    return config
  else:
    print("Config file not found!")
    sys.exit(1)

def update_config(args: dict, config: dict) -> None:
  if not args:
    print("Must provide args")
    sys.exit(1)

  global CONFIG_FILE
  images = config["images"]

  if args["mode"] == "create":
    if args["image"] not in images.items():
      images.update(
        {
          args["image"]: {
            "base_image": args['base_image'],
            "name": args['name'],
            "email": args['email']
          }
        }
      )

      with open(CONFIG_FILE, "w") as config_file:
        config_file.write(json.dumps(config))
    else:
      print("Image already exists!")
      sys.exit(1)
  elif args["mode"] == "delete":
    # TODO: implement deletion
    pass

  return

def create_image(args: dict) -> None:
  config = load_config()
  update_config(args, config)
  write_template(GITLAB_CI_SRC, GITLAB_CI_DST, config)

  if not os.path.isdir(args["image"]):
    os.makedirs(args["image"])

  write_template(DOCKERFILE_SRC, DOCKERFILE_DST.replace('name', args["image"]), load_config()["images"][args["image"]])

  return

def delete_image(args: dict) -> None:
  if not args:
    print("Must provide args")
    sys.exit(1)

  return

def update_all_images() -> None:
  config = load_config()
  write_template(GITLAB_CI_SRC, GITLAB_CI_DST, config)

  return

def setup(args: dict = dict()) -> None:
  if not args:
    print("Must provide args")
    sys.exit(1)

  if args["mode"] == "create":
    if args["base_image"] is None:
      base_image_prompt(args)

    if args["name"] is None:
      name_prompt(args)

    if args["email"] is None:
      email_prompt(args)

    create_image(args)
  elif args["mode"] == "delete":
    delete_image(args)
  elif args["mode"] == "update_all":
    update_all_images()
  else:
    print("Value for mode is invalid!")
    sys.exit(1)

  return

if __name__ == "__main__": # pragma: no cover
  setup(process_cli_args(setup_parser().parse_args()))
