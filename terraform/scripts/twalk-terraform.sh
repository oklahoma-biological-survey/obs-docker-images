#!/bin/sh -e

if [ "${DEBUG_OUTPUT}" = "true" ]; then
    set -x
    export TF_LOG=TRACE
fi

# Expose Gitlab specific variables to terraform since no -tf-var is available
# Usable in the .tf file as variable "CI_JOB_ID" { type = string } etc
export TF_VAR_CI_JOB_ID="${TF_VAR_CI_JOB_ID:-${CI_JOB_ID}}"
export TF_VAR_CI_COMMIT_SHA="${TF_VAR_CI_COMMIT_SHA:-${CI_COMMIT_SHA}}"
export TF_VAR_CI_JOB_STAGE="${TF_VAR_CI_JOB_STAGE:-${CI_JOB_STAGE}}"
export TF_VAR_CI_PROJECT_ID="${TF_VAR_CI_PROJECT_ID:-${CI_PROJECT_ID}}"
export TF_VAR_CI_PROJECT_NAME="${TF_VAR_CI_PROJECT_NAME:-${CI_PROJECT_NAME}}"
export TF_VAR_CI_PROJECT_NAMESPACE="${TF_VAR_CI_PROJECT_NAMESPACE:-${CI_PROJECT_NAMESPACE}}"
export TF_VAR_CI_PROJECT_PATH="${TF_VAR_CI_PROJECT_PATH:-${CI_PROJECT_PATH}}"
export TF_VAR_CI_PROJECT_URL="${TF_VAR_CI_PROJECT_URL:-${CI_PROJECT_URL}}"

# Use terraform automation mode (will remove some verbose unneeded messages)
export TF_IN_AUTOMATION=true

login() {
  cat <<EOF >> ~/.terraformrc
credentials "app.terraform.io" {
  token = "${TF_API_TOKEN}"
}
EOF
}

init() {
  echo "====== Logging in to Terraform Cloud ======"
  login
  echo "====== Initializing Terraform configuration ======"
  if [ ! -z "${TF_VALIDATE}" ]; then
    terraform init "${@}" -backend=false -input=false
  else
    terraform init "${@}" -backend-config=${WORKSPACE_FILE} -input=false
  fi
}

case "${1}" in
  "apply")
    init
    terraform "${@}" -input=false -auto-approve
  ;;
  "import")
    init
    terraform "${@}"
  ;;
  "init")
    shift
    init "${@}"
  ;;
  "plan")
    init
    terraform "${@}" -input=false
  ;;
  "output")
    init
    terraform "${@}"
  ;;
  "validate")
    init
    terraform "${@}"
  ;;
  *)
    terraform "${@}"
  ;;
esac
