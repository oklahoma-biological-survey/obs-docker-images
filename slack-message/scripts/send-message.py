from slack_sdk.webhook import WebhookClient
import sys

webhook = WebhookClient(sys.argv[1])

response = webhook.send(
  blocks = [
    {
      "type": "header",
      "text": {
          "type": "plain_text",
          "text": "New version of OBIS {} deployed".format(sys.argv[2])
      }
    },
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "Version `{}`\n{}".format(sys.argv[3], sys.argv[4])
      }
    },
    {
      "type": "divider"
    },
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "View OBIS"
      },
      "accessory": {
        "type": "button",
        "text": {
          "type": "plain_text",
          "text": "New OBIS Version"
        },
        "value": "view_new_obis_version",
        "url": "http://obis.ou.edu/",
        "action_id": "button-action"
      }
    }
  ]
)
